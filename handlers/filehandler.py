import logging

from base import BaseHandler


class FileHandler(BaseHandler):
    def get(self):
        kwargs = {
            'somedata' : 'read it beatch!',
        }
        for user in self.application.users:
            logging.warning(user)

        self.render('filehandler.html', **kwargs)
        logging.warning(self.request)
