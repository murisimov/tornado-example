#import logging

from base import BaseHandler


class DirHandler(BaseHandler):
    def get(self):
        self.write('dir(self.request)')
        self.write('<div><ul>')
        for attribute in dir(self.request):
            self.write('<li>' + attribute + '</li>')
        self.write('</ul></div>')
