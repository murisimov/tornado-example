import logging
from base import BaseHandler

class ChatHandler(BaseHandler):
    def get(self, **kwargs):
        kwargs = {
            'hostname': self.request.host,
        }
        self.render('chat.html', **kwargs)
