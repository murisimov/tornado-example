import logging

from base import BaseHandler
from namespace import authenticated


class MainHandler(BaseHandler):
    @authenticated
    def get(self):
        kwargs = {
            'my_data': "Let it be the main page.",
        }

        self.render('index.html', **kwargs)
