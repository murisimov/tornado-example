import logging
from base import BaseHandler


class AuthHandler(BaseHandler):
    def get(self, **kwargs):
        self.render('auth.html', error=None)

    def post(self):
        email = self.get_argument('email')
        password = self.get_argument('password')
        check_box = self.get_argument('check_box')
        overflow = "WARNING: %s has provided %s with length of %s symbols."
        if not email:
            error = "No email"
            self.render('auth.html', error=error)
        elif not password:
            error = "No password"
            self.render('auth.html', error=error)
        elif len(email) > 32:
            self.render('auth.html', error=None)
            remote_ip = self.request.remote_ip
            logging.warning( overflow % (remote_ip, "email", len(email)) )
        elif len(password) > 32:
            self.render('auth.html', error=None)
            remote_ip = self.request.remote_ip
            logging.warning( overflow % (remote_ip, "password", len(password)) )
        else:
            # Check if user exists
            if not self.db['users'].get(email):
                error = "No such user"
                self.render('auth.html', error=error)
            # Check if passwords match
            elif self.db['users'][email]['password'] != password:
                error = "Passwords do not match"
                self.render('auth.html', error=error)
            elif check_box:
                self.set_secure_cookie('example_user', email)
                self.redirect('/')
            else:
                self.set_secure_cookie('example_user', email, expires_days=None)
                self.redirect('/')

class LogOutHandler(BaseHandler):
    def get(self, **kwargs):
        self.clear_cookie('example_user')
        self.redirect('/auth')
