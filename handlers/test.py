import logging

from base import BaseHandler


class TestHandler(BaseHandler):
    def get(self):
        img = '<img src="http://www.networkforgood.com/wp-content/uploads/2015/08/bigstock-Test-word-on-white-keyboard-27134336.jpg">'
        self.write(img)
        req = self.request
        logging.info(req)
