import logging

from tornado.httpclient import HTTPClient, HTTPError, HTTPRequest
from tornado.ioloop import IOLoop
from tornado.escape import json_encode

from base import BaseHandler


class SlackHandler(BaseHandler):
    def get(self, **kwargs):
        kwargs = {
            'link': '',
            'message': '',
            'response': '',
        }
        self.render("slack.html", **kwargs)

    def post(self, **kwargs):
        message = self.get_argument('message')
        link = self.get_argument('link')
        if not link:
            logging.error("Please provide slack link!")
        elif not message:
            logging.error("Please provide some message!")
        else:
            http_client = HTTPClient()
            body = {"text": message}
            try:
                request = HTTPRequest(
                    link,
                    method="POST",
                    body=json_encode(body)
                )
                response = http_client.fetch(request)
                logging.info(response)
            except HTTPError as e:
                logging.error(str(e))
            else:
                kwargs = {
                    'link': link,
                    'message': message,
                    'response': response.reason,
                }
                self.render("slack.html", **kwargs)
            finally:
                http_client.close()
