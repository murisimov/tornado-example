import logging
from os.path import dirname

from base import BaseHandler


class FileHandler(BaseHandler):

    def get(self, *args, **kwargs):
        filename = self.request.uri.split('/')[-1]
        with open(dirname(__file__) + '/../files/' + filename) as f:
            content = f.read()

        self.write(content)
