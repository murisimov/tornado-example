import logging
from tornado.escape import json_decode, json_encode
from tornado.websocket import WebSocketHandler


class PollHandler(WebSocketHandler):
    @property
    def db(self):
        return self.application.db

    @property
    def listeners(self):
        return self.application.listeners

    def open(self):
        logging.info('Connection received from ' + self.request.remote_ip)
        self.listeners.append(self)

    def on_message(self, message):
        logging.info(json_decode(message))
        for client in self.listeners:
            shoutback = json_decode(message).upper()
            client.write_message(json_encode(shoutback))

    def on_close(self):
        logging.warning('Connection closed, ' + self.request.remote_ip)
        self.listeners.remove(self)
