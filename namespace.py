from functools import wraps
import shelve


db = shelve.open('storage.db', writeback=True)

def authenticated(func):
    @wraps(func)
    def wrapper(handler, *args, **kwargs):
        if handler.get_secure_cookie('example_user'):
            return func(handler, *args, **kwargs)
        return handler.redirect('/auth')
    return wrapper
