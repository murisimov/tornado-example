from os.path import join, dirname
from handlers.notfound import NotFoundHandler

settings = {
    'default_handler_class': NotFoundHandler,
    'template_path': join(dirname(__file__), 'templates'),
    'static_path': join(dirname(__file__), 'static'),
    'login_url': '/auth',
    'cookie_secret': 'example_secret',
}
