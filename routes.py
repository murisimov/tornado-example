from handlers.main import MainHandler
from handlers.file import FileHandler
from handlers.slack import SlackHandler
from handlers.filehandler import FileHandler
from handlers.dir import DirHandler
from handlers.test import TestHandler
from handlers.auth import AuthHandler, LogOutHandler
from handlers.chat import ChatHandler
from handlers.poll import PollHandler


routes = [
    (r"/", MainHandler),
    (r"/fs/(.*)", FileHandler),
    (r"/slack", SlackHandler),
    (r"/filehandler", FileHandler),
    (r"/dir", DirHandler),
    (r"/test", TestHandler),
    (r"/auth", AuthHandler),
    (r"/logout", LogOutHandler),
    (r"/chat", ChatHandler),
    (r"/poll", PollHandler),
]
