# -*- coding: utf-8 -*-

import logging
import sys

import tornado.ioloop
import tornado.web
from tornado.options import define, options, parse_command_line

import namespace
from routes import routes
from settings import settings


"""
 Define new option called "port" that has default value 8888.
 To run server on port e.g. 8877, call server.py with option --port=8877
"""
define("port", default=8888)


def main():
    " Parse command line arguments to see if there any options passed. "
    parse_command_line()

    # Log received arguments
    logging.info(sys.argv)

    app = tornado.web.Application(routes, **settings)

    """
      Add global variables to Application object
      for later use in e.g. RequestHandler's.
    """
    app.db = namespace.db
    app.listeners = []

    app.listen(options.port)
    try:
        logging.info("server started")
        tornado.ioloop.IOLoop.current().start()
    except:
        app.db.close
        logging.info("server stopped")

if __name__ == "__main__":
    main()
