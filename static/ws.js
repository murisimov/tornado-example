$(document).ready(function () {
    if ("MozWebSocket" in window) {
        WebSocket = MozWebSocket;
    }
    if (WebSocket) {
        //var ws = new WebSocket(`ws://${hostname}/poll`);
        var ws = new WebSocket(`ws://${hostname}/poll`);

        function send(message) {
            ws.send(JSON.stringify(message));
        }

        ws.onopen = function() {
            console.log(`Server connection established, ${hostname}.`);
        };

        ws.onmessage = function(evt) {
            let data = $.parseJSON(evt.data);
            console.log(data);
            $('#field').append('<p>' + data + '</p>');
        };

        $('#submit').on('click', (function () {
            let message = $('#text').val();
            $('#text').val(function() {return ''});
            send(message);
        }));

    }
});
